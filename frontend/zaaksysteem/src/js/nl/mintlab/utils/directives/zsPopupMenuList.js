/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem')
		.directive('zsPopupMenuList', [ function ( ) {
			
			return {
				require: '^zsPopupMenu',
				link: function ( scope, element, attrs, zsPopupMenu ) {
					zsPopupMenu.setList(element);
					
					scope.$on('$destroy', function ( ) {
						zsPopupMenu.setList(null);
					});
				}
			};
						
		}]);
})();