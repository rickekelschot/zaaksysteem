/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.CaseTabController', [ '$scope', function ( $scope ) {
			
			$scope.$watch('notificationCount', function ( ) {
				if($scope.notificationCount) {
					$scope.setNotificationCount($scope.phaseId, $scope.notificationCount);
				}
			});
			
		}]);
	
})();