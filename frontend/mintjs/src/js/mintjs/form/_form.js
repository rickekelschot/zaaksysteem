/*global angular*/
(function ( ) {
	
	angular.module('MintJS.form', [
		'MintJS.core'
	]);
	
})();