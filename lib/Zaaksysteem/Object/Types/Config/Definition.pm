package Zaaksysteem::Object::Types::Config::Definition;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Config::Definition - Data wrapper for
C<config/definition> objects

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Zaaksysteem::Types qw(JSONBoolean);
use Moose::Util::TypeConstraints qw[enum];
use Syzygy::Types qw[Name];

=head1 METHODS

=head2 type

Returns the object type string C<config/definition>.

=cut

override type => sub { return 'config/definition' };

=head1 ATTRIBUTES

=head2 required

Boolean value to indicate that the corresponding item requires a value

=cut

has required => (
    is      => 'ro',
    isa     => JSONBoolean,
    default => 0,
    lazy    => 1,
    traits  => [qw[OA]],
    coerce  => 1,
    label   => 'Requiredness',
);

=head2 mvp

Multi-value-parameter. This determines if the value of the config item
can be zero or more.

=cut

has mvp => (
    is      => 'ro',
    isa     => JSONBoolean,
    label   => 'Multi value paramater',
    default => 0,
    lazy    => 1,
    coerce  => 1,
    traits => [qw[OA]]
);

=head2 mutable

Boolean value to indicate that the config item can be changed by an admin.
Primarly there for subscription based services.

=cut

has mutable => (
    is      => 'ro',
    isa     => JSONBoolean,
    label   => 'Mutable',
    default => 1,
    lazy    => 1,
    coerce  => 1,
    traits => [qw[OA]]
);


=head2 deprecated

Boolean value to indicate that the config item is candidate for removal
in later versions of Zaaksysteem.

=cut

has deprecated => (
    is      => 'ro',
    isa     => JSONBoolean,
    label   => 'Deprecated',
    default => 0,
    lazy    => 1,
    coerce  => 1,
    traits => [qw[OA]]
);

=head2 key

Defines the name of the config item/definition.

=cut

has config_item_name => (
    is       => 'rw',
    isa      => Name,
    label    => 'Configuratie item naam',
    traits   => [qw[OA]],
    required => 1,
);

=head2 value_type_name

Value type name for values

=cut

has value_type_name => (
    is        => 'rw',
    isa       => Name,
    label     => 'Interne naam van waardetype',
    traits    => [qw[OA]],
    predicate => 'has_value_type_name'
);

=head2 value_type

Value sub-type definition

=cut

has value_type => (
    is        => 'rw',
    isa       => 'HashRef',
    label     => 'Waardetype definitie',
    traits    => [qw[OA]],
    predicate => 'has_value_type',
    required  => 0,
);

=head2 cardinality

Indicates the cardinality of (size of set of values) the config item.

Can be one of C<one>, C<one-or-more>, C<zero-or-more>.

=cut

has cardinality => (
    is      => 'rw',
    isa     => enum([qw[one one-or-more zero-or-more]]),
    label   => 'Kardinaliteit van het configuratie item',
    traits  => [qw[OA]],
    default => 'one'
);

=head2 default

Default (reset) value of the configuration item instance for this
definition.

=cut

has default => (
    is     => 'rw',
    label  => 'Standaardwaarde van het configuratie item',
    traits => [qw[OA]]
);

=head2 label

Defines the label for the config.

=cut

has label => (
    is     => 'rw',
    isa    => 'Str',
    label  => 'Beschrijving van het configuratie item',
    traits => [qw[OA]]
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
