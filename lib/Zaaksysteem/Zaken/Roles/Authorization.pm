package Zaaksysteem::Zaken::Roles::Authorization;

use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::Authorization - Permission check infrastructure

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Zaaksysteem::Constants qw[
	ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX
];

sub _eval_case_action_authorization_matrix {
    my ($self, $user, $user_perm_checker, $context, $action) = @_;

    my $matrix = ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX;
    my $results = {};

    # First eval default context
    if ($context ne '_' and my $actions = $matrix->{ _ }) {
        my $_results = {};

        if (my $rules = $actions->{ _ }) {
            $_results->{ _ } = $self->_assert_auth_rules(
                $rules,
                $user,
                $user_perm_checker
            );
        }

        if (my $rules = $actions->{ $action }) {
            $_results->{ $action } = $self->_assert_auth_rules(
                $rules,
                $user,
                $user_perm_checker
            );
        }

        $results->{ _ } = $_results;
    }

    my $actions = $matrix->{ $context };
    my $_results = {};

    unless (defined $actions) {
        throw('zaak/authorization/context_does_not_exist', sprintf(
            'Requested case authorization context "%s" does not exist.',
            $context
        ));
    }

    if (my $rules = $actions->{ _ }) {
        $_results->{ _ } = $self->_assert_auth_rules(
            $rules,
            $user,
            $user_perm_checker
        );
    }

    my $rules = $actions->{ $action };

    unless (defined $rules) {
        throw('zaak/authorization/action_does_not_exist', sprintf(
            'Requested case authorization action "%s" does not exist in context "%s"',
            $action,
            $context
        ));
    }

    $_results->{ $action } = $self->_assert_auth_rules(
        $rules,
        $user,
        $user_perm_checker
    );

    $results->{ $context } = $_results;

    return $results;
}

sub _assert_auth_rules {
    my ($self, $rules, $user, $user_perm_checker) = @_;

	for my $rule (@{ $rules }) {
        if (my $scope = $rule->{ scope }) {
            next unless $user_perm_checker->($self, @{ $scope });
        }

        my $cond_fail;

        for my $condition (@{ $rule->{ conditions } }) {
            my $predicate = $self->can(sprintf('ddd_%s', $condition));

            $predicate //= $self->_get_auth_predicate($condition);

            unless (defined $predicate) {
                throw('zaak/authorization/predicate_does_not_exist', sprintf(
                    'Given authorization rule condition "%s" does not have a predicate',
                    $condition
                ));
            }

            next if $predicate->($self, $user);

            $cond_fail = 1;

            # Short-circuit rest of tests
            last;
        }

        next if $cond_fail;

        return sprintf(
            'in scope(%s), conditions(%s)',
            join(',', @{ $rule->{ scope } || [] }),
            join(',', @{ $rule->{ conditions } || [] })
        );
    }

    throw('zaak/authorization/user_not_authorized', sprintf(
        'Authorization condition(s) failed for user "%s" on case %s',
        $user->display_name,
        $self->id
    ));
}

sub _get_auth_predicate {
    # Empty for now, this is the intended hook for predicates that
    # don't resolve to a 'ddd_my_predicte' method, or need some other logic
    # for its retrieval.
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
