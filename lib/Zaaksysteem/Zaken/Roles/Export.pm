package Zaaksysteem::Zaken::Roles::Export;
use Moose::Role;

=head1 METHODS

=head2 export_files

Export files for documentenlijst. Search results > Exporteren > Documentenlijst

=cut

sub export_files {
    my $self = shift;

    return map {[
        $_->filepath,
        $_->filestore->mimetype,
        $_->date_created->strftime('%d-%m-%Y %H:%M:%S'),
        'zaakbehandeling',
        $self->status eq 'open' ? 'Open' : 'Gearchiveerd',
        $_->document_status,
        $self->id,
        $_->version,
    ]} $self->active_files->all;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
