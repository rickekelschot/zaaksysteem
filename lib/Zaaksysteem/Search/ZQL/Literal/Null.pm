package Zaaksysteem::Search::ZQL::Literal::Null;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Literal';

has value => ( is => 'ro', isa => 'Undef' );

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

