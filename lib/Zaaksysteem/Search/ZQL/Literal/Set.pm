package Zaaksysteem::Search::ZQL::Literal::Set;

use Moose;

use Zaaksysteem::Search::Term::Set;

extends 'Zaaksysteem::Search::ZQL::Literal';

has '+value' => ( isa => 'ArrayRef' );

sub dbixify {
    return Zaaksysteem::Search::Term::Set->new(
        values => [ map { $_->dbixify } @{ shift->value } ]
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 dbixify

TODO: Fix the POD

=cut

