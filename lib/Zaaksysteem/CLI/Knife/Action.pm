package Zaaksysteem::CLI::Knife::Action;

use Moose;

use base 'Exporter';

our @EXPORT     = qw[register_knife register_category register_action];
our @EXPORT_OK  = qw[$actions];

our $actions = {};

=head1 NAME

Zaaksysteem::CLI::Knife::Actions - Helper tools for zsknife

=head1 SYNOPSIS

=head1 DESCRIPTION

Actions for registering actions, categories and knifes for zsknife

=cut

=head1 ACTIONS


=head2 register_knife

Arguments: $STRING_NAME

Return value: $TRUTH (or die)

    register_knife      'controlpanel' => (
        description => "Controlpanel related functions"
    );

Registers a new toolset for knife.

=cut

sub register_knife($%) {
    my $knife       = shift;
    my (%opts)      = @_;

    $actions->{$knife} ||= {
        description     => $opts{description},
        categories      => {}
    };
}

=head2 register_category

Arguments: $STRING_NAME

Return value: $TRUTH (or die)

    register_category   'cp' => (
        knife           => 'controlpanel',
        description     => "Controlpanel actions"
    );

Registers a new category below the given knife

=cut

sub register_category($%) {
    my $category    = shift;
    my (%opts)      = @_;

    $actions->{ $opts{knife} }->{categories}->{ $category } ||= {
        description     => $opts{description},
        actions         => {}
    };
}

=head2 register_action

Arguments: $STRING_NAME

Return value: $TRUTH (or die)

    register_action     'hello' => (
        knife           => 'controlpanel',
        category        => 'cp',
        description     => 'Show the world',
        run             => sub {
            my $self        = shift;
            my (@params)    = @_;

            return 'Hello World!'
        }
    );

Registers a new action below the given category and knife

=cut

sub register_action($%) {
    my $action      = shift;
    my (%opts)      = @_;

    die("Category $opts{category} not specified") unless $actions->{ $opts{knife} }->{categories}->{ $opts{category} };

    $actions->{ $opts{knife} }->{categories}->{ $opts{category} }->{actions}->{$action} = {
        code            => $opts{run},
        description     => $opts{description},
    };
}

1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
