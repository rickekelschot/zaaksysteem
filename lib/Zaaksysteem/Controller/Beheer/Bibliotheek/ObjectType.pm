package Zaaksysteem::Controller::Beheer::Bibliotheek::ObjectType;

use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

sub search
    : Chained('/beheer/bibliotheek/base')
    : PathPart('objecttype/search')
    : Args()
{
    my ( $self, $c )        = @_;

    return unless $c->req->is_xhr($c);
    $c->stash->{nowrapper} = 1;

    $c->stash->{bib_type}   = "objecttype";

    $c->stash->{bib_cat}        = $c->model('DB::BibliotheekCategorie')->search(
        {
            'system'    => undef,
        },
        {
            order_by    => ['pid','naam']
        }
    );

    my $params = $c->req->params();

    if ($params->{search}) {
        ### Return json response with results
        my $json = [];

        # Only return custom types.
        my $search_query = {
            'me.object_type' => 'type',
        };

        if ($params->{naam}) {
            $search_query->{'me.search_term'} = {
                'ilike' => '%' . lc($params->{naam}) . '%',
            };
        }

        if($params->{bibliotheek_categorie_id}) {
            $search_query->{'me.bibliotheek_categorie_id'} = $params->{bibliotheek_categorie_id};
        }

        my $types = $c->model('DB::ObjectBibliotheekEntry')->search(
            $search_query,
            {
                prefetch => ['object_uuid', 'bibliotheek_categorie_id']
            }
        );

        while (my $type = $types->next) {
            my $object = $c->model('Object')->inflate_from_row($type->object_uuid);

            push(@{ $json },
                {
                    'naam'          => $object->name,
                    'invoertype'    => 'object',
                    'categorie'     => $type->bibliotheek_categorie_id->naam,
                    'id'            => $type->id,
                    'uuid'          => $object->id,
                }
            );
        }
        $c->stash->{json} = [ sort { $a->{naam} cmp $b->{naam} } @$json ];
        $c->detach('Zaaksysteem::View::JSONlegacy');
    }

    $c->stash->{template} = 'widgets/beheer/bibliotheek/search.tt';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 search

TODO: Fix the POD

=cut

