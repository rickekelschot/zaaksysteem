package Zaaksysteem::Zorginstituut::Provider;
use Moose;
use namespace::autoclean;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zorginstituut::Provider - A provider class

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

use BTTW::Tools;
use BTTW::Tools::UA;
use HTTP::Request;
use HTTP::Headers;
use Zaaksysteem::Version;

=head1 ATTRIBUTES

=head2 ua

An L<LWP::UserAgent> object, build for you

=cut

has ua => (
    is      => 'ro',
    isa     => 'LWP::UserAgent',
    builder => '_build_useragent',
);

=head2 timeout

The timeout for the L<LWP::UserAgent> object.

=cut

has timeout => (
    is      => 'ro',
    isa     => 'Int',
    default => 15,
);

=head2 di01_endpoint

An endpoint for di01 messages

=cut

has di01_endpoint => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 ca_certificate

Use the given CA certificate

=cut

has ca_certificate => (
    is        => 'ro',
    isa       => 'Defined',
    predicate => 'has_ca_certificate'
);

=head2 client_public_key

Use the given client public key

=cut

has client_public_key => (
    is        => 'ro',
    isa       => 'FileHandle',
    predicate => 'has_client_public_key'
);

=head2 client_private_key

Use the given client private key

=cut

has client_private_key => (
    is        => 'ro',
    isa       => 'FileHandle',
    predicate => 'has_client_private_key',
);

=head2 build_request

Build a request

=cut

define_profile build_request => (
    required => {
        endpoint      => 'Str',
        xml           => 'Str',
        'soap-action' => 'Str',
    },
);

sub build_request {
    my $self = shift;
    my $opts = assert_profile({@_})->valid;

    my $req = HTTP::Request->new(
        POST => $opts->{endpoint},
        HTTP::Headers->new(
            Content_Type => 'text/xml; charset=UTF-8',
            SOAPAction => $opts->{'soap-action'},
        ),
        $opts->{xml},
    );

    if ($self->log->is_trace) {
        $self->log->trace("Send the following request " . $req->as_string);
    }

    return $req;
}

=head2 send_request

Send a HTTP::Request with the defined user agent

=cut

sig send_request => 'HTTP::Request';

sub send_request {
    my $self = shift;
    my $req  = shift;

    my $res = $self->ua->request($req);
    return $self->assert_http_response($res);
}

sub assert_http_response {
    my ($self, $response) = @_;

    if ($response->is_success) {
        return $response->decoded_content;
    }

    if ($self->log->is_trace) {
        $self->log->trace("Got the following response " . $response->as_string);
    }

    throw(
        "zorginstituut/provider/response/unsuccesful",
        sprintf(
            "Response from %s is not succesful (%s): %s",
            $response->request->uri, $response->status_line,
            $response->decoded_content
        )
    );
}

=head1 PRIVATE METHODS and BUILDERS

=head2 _build_useragent

Returns an L<LWP::UserAgent> object created by L<BTTW::Tools::UA::new_user_agent>.

=cut

sub _build_useragent {
    my $self = shift;

    my %args = (
        agent   => "Zaaksysteem/$VERSION",
        timeout => $self->timeout,
    );

    $args{ca_cert} = "" . $self->ca_certificate if $self->has_ca_certificate;

    if ($self->has_client_private_key) {
        $args{client_cert} = "" . $self->client_private_key;
        $args{client_key}  = "" .  $self->client_private_key;
    }

    return new_user_agent(%args);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
