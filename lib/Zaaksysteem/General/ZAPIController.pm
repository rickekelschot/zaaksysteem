package Zaaksysteem::General::ZAPIController;

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::General::ZAPIController - ZAPI base controller

=head1 DESCRIPTION

This L<Catalyst::Controller>-deriving base class for ZAPI controllers ensures
L<Zaaksysteem::ActionRole::ZAPI> is applied to all actions in the package.

Deriving a controller from this package also ensures
L<Zaaksysteem::Controller::Page/begin> is called as part of the execution
chain.

=cut

use BTTW::Tools;

BEGIN {
    extends 'Catalyst::Controller';
    with 'MooseX::Log::Log4perl';
}

__PACKAGE__->config(
    action_roles    => ['ZAPI'],
    dv_profiles     => {},
    default_view    => 'ZAPI',
);

=head1 ACTIONS

=head2 begin

This init action forwards the execution to
L<Zaaksysteem::Controller::Page/begin> so all security checks there are hit.

Also sets Catalyst's C<default_view> to C<ZAPI> for ease of use in the ZAPI
infrastructure.

=cut

sub begin : Private {
    my ($self, $c) = @_;

    $c->forward('/page/begin');

    $c->stash->{current_view} = 'ZAPI';
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
