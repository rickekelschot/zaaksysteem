package Zaaksysteem::DB::Component::Logging::Case::EarlySettle;

use Moose::Role;

sub onderwerp {
    my $self = shift;

    sprintf(
        'Zaak %s is vroegtijdig afgehandeld: %s, onder opgave van de volgende reden: %s',
        $self->data->{ case_id },
        $self->pretty_label,
        $self->data->{ reason },
    );
}

sub event_category { 'case-mutation'; }

sub pretty_label {
    my $self = shift;

    my $result_name  = $self->data->{ case_result } // '';
    my $result_label = $self->data->{ case_result_label };

    my $pretty_label = $result_name . ( defined $result_label ? " ($result_label)" : '' );

    return $pretty_label;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

