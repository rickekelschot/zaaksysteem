package Zaaksysteem::DB::Component::Logging::Admin::Catalog::FolderEntriesMoved;
use Moose::Role;

=head2 onderwerp

Generate the main description for this event.

=cut

sub onderwerp {
    my $self = shift;

    my $data = $self->data;

    return sprintf(
        "%d item%s verplaatst naar %s",
        $data->{moved_item_count},
        $data->{moved_item_count} != 1 ? 's' : '',
        defined $data->{destination_folder_name}
            ? "'$data->{destination_folder_name}'"
            : "de hoofdmap"
    );
}

=head2 event_category

Category this event is in.

=cut

sub event_category { 'system' }

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
