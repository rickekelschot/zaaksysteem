package Zaaksysteem::DB::Component::ZaaktypeAuthorisation;

use strict;
use warnings;

use base qw/DBIx::Class::Row/;

sub security_identity {
    my $self = shift;

    return (
        position => sprintf('%s|%s', $self->ou_id, $self->role_id)
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 security_identity

TODO: Fix the POD

=cut

