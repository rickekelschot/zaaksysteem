package Zaaksysteem::API::v1::ResultSet;

use Moose;
use namespace::autoclean -except => 'meta';

use BTTW::Tools;

=head1 NAME

Zaaksysteem::API::v1::ResultSet - DBIx::Class::ResultSet Sets

=head1 SYNOPSIS

    my $set = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $c->model('DB::NatuurlijkPersoon')->search(...),
    );

    $set->init_paging($c->request);

    # Serialize the dataview with a page.
    $c->stash->{ result } = $set;

=head1 DESCRIPTION

This class creates a set of records from a given ResultSet

=head1 ATTRIBUTES

=head2 iterator

Holds an L<DBIx::Class::ResultSet> object that is to be used as a base
resultset of the resource.

=cut

has iterator => (
    is => 'rw',
    # isa => 'DBIx::Class::ResultSet',
    required => 1
);

=head2 page

State field that determines the current page within the dataview is to be
used. Defaults to page C<1>.

=cut

has page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 1
);

=head2 rows_per_page

Limit on the maximum amount of entries/items/rows on a single page. Defaults
to C<10>.

=cut

has rows_per_page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 10
);

=head2 allow_rows_per_page

Limit on the maximum amount of entries/items/rows which can be set via the
"rows_per_page" query parameter. Defaults to C<50>.

=cut

has allow_rows_per_page => (
    is => 'rw',
    isa => 'Int',
    required => 1,
    default => 250,
);

=head2 uri

Holds an L<URI> object that is used as a base to provide paged access to the
resource.

=cut

has uri => (
    is => 'rw',
    isa => 'URI'
);

=head1 METHODS

=head2 init_paging

This method initializes the set with the current 'view', according to the
request being made.

=head3 Signature

C<< Catalyst::Request => Zaaksysteem::API::v1::ResultSet >>

=cut

sig init_paging => 'Catalyst::Request => Zaaksysteem::API::v1::ResultSet';

sub init_paging {
    my $self = shift;
    my $res = shift;
    my $params = $res->query_params;

    $self->uri($res->uri->clone);

    if (exists $params->{ page }) {
        $self->page($params->{ page });
    }

    if (
        exists $params->{ rows_per_page } &&
        int($params->{ rows_per_page }) <= $self->allow_rows_per_page
    ) {
        $self->rows_per_page($params->{ rows_per_page });
    }

    return $self;
}

=head2 mangle_uri

Produces a string representation of an URI that can be passed to the frontend
to retrieve a view of the object's data set to a specific page.

=head3 Signature

C<< Maybe[Int] => Maybe[Str] >>

=head3 Example call

    my $uri = $set->mangle_uri($next_page);

=cut

sub mangle_uri {
    my $self = shift;
    my $page = shift;

    return undef unless defined $page;

    my $uri = $self->uri->clone;

    $uri->query_param(page => $page);

    return $uri->as_string;
}

=head2 build_iterator

This method returns a paged L<DBIx::Class::ResultSet>

=head3 Signature

C<< => DBIx::Class::ResultSet >>

=head3 Example call

    my $iter = $set->build_iterator;

=cut

sub build_iterator {
    my $self = shift;

    return $self->iterator->search_rs(undef,
        {
            rows => $self->rows_per_page,
            page => $self->page
        }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
