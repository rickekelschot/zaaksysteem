package Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPAPI;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPAPI - Abstract role for SOAP API interfaces/modules

=head1 DESCRIPTION

Consumers must implement the following functions:
C<is_valid_soap_api> and C<check_client_certificate>

=head1 SYNOPSIS

    package ZS::Backend::Sysin::Modules::Foo;
    with 'Zaaksysteem::Backend::Syin::Modules::Roles::SOAPAPI';

    sub is_valid_soap_api {
        my $self = shift;
        my $params = assert_profile({@_})->valid;

        # actual code here

       return 1; # or 0
    }

    sub check_client_certificate {
        my $self = shift;
        my $params = assert_profile({@_})->valid;

        # actual code here

        return 1; # or 0
    }


=cut

requires qw(
    is_valid_soap_api
    check_client_certificate
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
