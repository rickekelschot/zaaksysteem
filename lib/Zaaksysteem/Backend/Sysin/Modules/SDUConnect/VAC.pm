package Zaaksysteem::Backend::Sysin::Modules::SDUConnect::VAC;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use BTTW::Tools;

use XML::XPath;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw[
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
    Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient
];

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::SDUConnect::VAC - FAQ object
synchronisation via SDU Connect

=head1 DESCRIPTION

=head1 CONSTANTS

=head2 INTERFACE_ID

C<Str[sdu_connect_vac]> - Claim interface module name

=cut

use constant INTERFACE_ID => 'sdu_connect_vac';

=head2 INTERFACE_CONFIG_FIELDS

C<ArrayRef[Zaaksysteem::ZAPI::Form::Field]> - Collection of form fields for
the interface's main fieldset

=cut

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_url',
        type => 'text',
        label => 'REST API URL',
        required => 1,
        description => 'URL van de SDU Connect REST API'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_account_id',
        type => 'text',
        label => 'Account ID',
        required => 1,
        description => 'Door SDU uitgegeven Account ID'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_collection_id',
        label => 'Collection ID',
        type => 'text',
        required => 1,
        description => 'Collection ID van de te synchroniseren collectie vragen'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_api_version',
        label => 'Delta API versie',
        type => 'select',
        required => 1,
        default => '1.0',
        data => {
            options => [
                { value => '1.0', label => 'v1.0' },
            ]
        }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_last_sync',
        label => 'Laatste synchronisatie',
        description => 'Datum en tijd van de laatste synchronisatie',
        type => 'display',
        data => { template => "<[field.value | date:'dd-MM-yyyy HH:MM']>" }
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_next_sync',
        label => 'Volgende synchronisatie',
        description => 'Datum en tijd van de volgende (geplande) synchronisatie',
        type => 'display',
        data => { template => "<[field.value | date:'dd-MM-yyyy HH:MM']>" }
    )
];

=head2 ATTRIBUTE_LIST

C<ArrayRef[HashRef]> - Hardcoded list of VAC data source fields

=cut

use constant ATTRIBUTE_LIST => [
    {
        slug => 'id',
        external_name => 'SDU Entry ID',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'question',
        external_name => 'Vraag',
        attribute_type => 'magic_string',
        objecttype_attribute => 1,
    },
    {
        slug => 'answer',
        external_name => 'Antwoord',
        attribute_type => 'magic_string',
        objecttype_attribute => 1,
    },
    {
        slug => 'keywords',
        external_name => 'Trefwoorden',
        attribute_type => 'magic_string',
        objecttype_attribute => 1
    },
    {
        slug => 'product_type',
        external_name => 'Objecttype voor productrelaties',
        attribute_type => 'objecttype',
        handles_type => 'pdc'
    }
];

=head2 MODULE_SETTINGS

C<HashRef> - Attribute key/value pairs for module instantiation

=cut

use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'SDU Connect VAC',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [qw[file]],
    is_multiple => 1,
    is_manual => 1,
    allow_multiple_configurations => 1,
    is_objecttype_interface => 1,
    has_attributes => 1,
    test_interface => 1,
    attribute_list => ATTRIBUTE_LIST,

    active_state_callback => sub {
        return shift->install_scheduled_job(@_);
    },

    test_definition => {
        description => '',
        tests => [
            {
                id => 1,
                label => 'Test verbiding met SDU Connect',
                name => 'test_connection',
                method => 'test_connection',
                description => 'Test verbinding met gespecificeerde API'
            },
            {
                id => 2,
                label => 'Test diff verzoek',
                name => 'test_get_delta',
                method => 'test_get_delta',
                description => 'Test het ophalen van een VAC diff'
            }
        ]
    },

    trigger_definition => {
        delta_sync => {
            method => 'delta_sync',
            update => 1
        },

        create_object => {
            method => 'create_object',
            update => 1
        },

        update_object => {
            method => 'update_object',
            update => 1
        },

        delete_object => {
            method => 'delete_object',
            update => 1
        }
    }
};

=head1 ATTRIBUTES

=head1 METHODS

=cut

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig(%{ MODULE_SETTINGS() });
};

=head2 entry_type

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns the hard-coded string C<vac>.

=cut

sub entry_type { 'vac' }

=head2 entry_nodeset

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns a L<XML::XPath::NodeSet>.

=cut

sub entry_nodeset {
    my $self = shift;
    my $xpath = shift;

    return $xpath->find('/vac:vacs/vac:vac');
}

=head2 entry_relations

=cut

sub entry_relations {
    my $self = shift;
    my $xpath = shift;
    my $entry = shift;

    my @relations;

    my $vacs = $xpath->find('./vac:body/vac:verwijzingVac', $entry);
    my $pdcs = $xpath->find('./vac:body/vac:verwijzingProduct', $entry);

    for ($vacs->get_nodelist) {
        push @relations, {
            id => "" . $xpath->findvalue('./@resourceIdentifier', $_),
            label => "" . $_->string_value,
            type => 'vac'
        };
    }

    for ($pdcs->get_nodelist) {
        push @relations, {
            id => "" . $xpath->findvalue('./@resourceIdentifier', $_),
            label => "" . $_->string_value,
            type => 'pdc'
        };
    }

    return @relations;
}

=head2 entry_id

=cut

sub entry_id {
    my $self = shift;
    my $xpath = shift;
    my $entry = shift;

    my $owmskern = $xpath->find('./vac:meta/vac:owmskern', $entry)->pop;

    return "" . $xpath->findvalue('./dcterms:identifier', $owmskern);
}

=head2 entry_values

Implements interface required by
L<Zaaksysteem::Backend::Sysin::Modules::Roles::SDUConnectClient>.

Returns a mapping of internal slugs to values, as derived from a VAC entry.

=cut

sub entry_values {
    my $self = shift;
    my $xpath = shift;
    my $entry = shift;

    my $kanaal = $xpath->find('./vac:body/vac:kanaal', $entry)->pop;
    my $answer = $xpath->find('./vac:antwoord', $kanaal)->pop;

    my $answer_text = $xpath->findvalue('./vac:antwoordProductVeld', $answer);

    unless (length $answer_text) {
        $answer_text = $xpath->findvalue('./vac:antwoordTekst', $answer);
    }

    my $trefwoorden = $xpath->find(
        './vac:meta/vac:vacmeta/vac:trefwoord',
        $entry
    );

    my @keywords = map {
        $_->string_value
    } $trefwoorden->get_nodelist;

    return {
        question => "" . $xpath->findvalue('./vac:vraag', $kanaal),
        answer => "" . $answer_text,
        keywords => join(' ', @keywords)
    };
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Sysin::Modules::SDUConnect>, L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
