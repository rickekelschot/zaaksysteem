package Zaaksysteem::StUF::AOA;

use Moose::Role;

use constant ADDRESS_MAP            => {
    'wpl.identificatie'                     => 'woonplaats_identificatie',
    'wpl.woonplaatsNaam'                    => 'woonplaats_naam',

    'opr.identificatie'                     => 'openbareruimte_identificatie',
    'gor.openbareRuimteNaam'                => 'openbareruimte_naam',

    'identificatie'                         => 'nummeraanduiding_identificatie',
    'ingangsdatumObject'                    => 'nummeraanduiding_begindatum',
    'huisnummer'                            => 'nummeraanduiding_huisnummer',
    'huisletter'                            => 'nummeraanduiding_huisletter',
    'huisnummertoevoeging'                  => 'nummeraanduiding_huisnummertoevoeging',
    'postcode'                              => 'nummeraanduiding_postcode',
    'inOnderzoek'                           => 'nummeraanduiding_inonderzoek',
    'num.status'                            => 'nummeraanduiding_status',
};

=head2 METHODS

=head2 get_params_for_natuurlijk_persoon

Gets a set of params for manipulating natuurlijk_persoon

=cut

sub get_params_for_adr {
    my $self            = shift;

    my $params          = {};
    my $object_params   = $self->as_params->{AOA};

    for my $key (keys %{ ADDRESS_MAP() }) {
        my $object_key  = $key;
        my $object_value;

        $object_value = $object_params->{ $object_key };

        $params->{ADDRESS_MAP->{$key}} = $object_value;
    }

    if (
        $object_params->{'isVan'} &&
        $object_params->{'isVan'}->[0]->{TGO}
    ) {
        $params->{verblijfsobject_identificatie} = $object_params->{'isVan'}->[0]->{TGO}->{identificatie};
        $params->{verblijfsobject_status}        = 'Verblijfsobject in gebruik';
    }

    return $params;
}


1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 get_params_for_adr

TODO: Fix the POD

=cut

