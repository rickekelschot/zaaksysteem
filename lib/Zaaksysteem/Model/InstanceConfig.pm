package Zaaksysteem::Model::InstanceConfig;

use Moose;
use namespace::autoclean;

use File::Spec::Functions qw[catfile];

extends 'Catalyst::Model::Adaptor';

__PACKAGE__->config(
    class => 'Zaaksysteem::InstanceConfig::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::InstanceConfig - Catalyst model factory for
L<Zaaksysteem::InstanceConfig::Model>.

=head1 SYNOPSIS

    my $config_model = $c->model('InstanceConfig');

=head1 METHODS

=head2 prepare_arguments

Prepares arguments for L<<Zaaksysteem::InstanceConfig::Model->new>> based on
Catalyst configuration.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        zaaksysteem_config => { %{ $c->config } },
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
