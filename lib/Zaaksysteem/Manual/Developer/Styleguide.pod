package Zaaksysteem::Example::Module;
use Moose;

# External first and sorted
use BTTW::Tools;
use Data::Dumper; # we don't want to see this in our commits btw
use XML::Compile::Schema;

# Zaaksysteem goes below
use Zaaksysteem::My::Package;

with 'MooseX::Log::Log4perl'; # we like logging

=head1 NAME

Zaaksysteem::Example::Module - An example module

=head1 SYNOPSIS

    use ZS::Example::Module;

    my $foo = ZS::Example::Module->new(
        # explain the default usage here
        # make sure we can copy/paste the code
        # and have a working example
    );

    # some handy functions or bla
    my $requestor = $case->set_requestor(%params);


=head1 DESCRIPTION

This module tries to describe what we want to see in our Moose-modules.
POD is important and we want to have a consistent style in writing modules.
Opinions about where POD goes differs, some like it at the bottom, belown the
C<__END__> tags, others like it interleaved. Because we are writing code, we
tend not to run perldoc Our::Module, but open it in our editor. Therefore we
silently want to have all the POD interleaved.

=head1 ATTRIBUTES

=head2 schema

B<Type>: L<Zaaksysteem::Schema>

B<Default>: gets created for you

B<Required>: yes

Maybe some description when attribute is not directly obvious. Also instructions about subclassing, extending builders, etc.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head1 METHODS

=head2 set_requestor

    my $requestor = $case->set_requestor(
        requestor => $subject_object        # required
    );

B<Return value>: L<Zaaksysteem::DB::Subject>

Sets a requestor for the current case. Explain non obvious behaviour and/or arguments or return value.

=cut

# do this
define_profile bar => (
    required => {
        baz => "Foo::Bar::Ish",
    },
);

# or use sig
sig bar => "Foo::Bar::Ish"

sub bar {
    my $self = shift;
    my $params = assert_profile({ @_ })->valid;

    # If your code does something funny, or you do something that breaks our
    # regular programming, explain why. But don't say
    #
    # Looping
    for my $loop (qw(foo bar baz)) {
    }
    # coz we can see that.
};

=head1 PRIVATE METHODS

You break it, you buy it

=head2 _touch

    my $success = $case->_touch();

B<Return value>: Boolean

Every private method starts with an underscore and has proper documentation. Explain non obvious behaviour
and/or arguments or return value. We strongly suggest to use C<define_profile> or C<sig>.

=cut

__PACKAGE__->meta->make_immutable;

__END__

=head1 TESTS

Tests can be found in L<TestFor::General::Example::Module>. You can test this module by running

    ./zs_prove t/lib/TestFor/General/Example/Module.pm

=head1 SEE ALSO

=over

=item L<http://perldoc.perl.org/perlmodstyle.html#POD>

More information about how to POD your modules.

=item L<http://perldoc.perl.org/perlpodstyle.html>

More information and guidelines for POD-ing your module.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
