=head1 NAME

Zaaksysteem::Manual::API::V1::Types::Config::Category - Type definition for
config/category objects

=head1 DESCRIPTION

This page documents the serialization of C<config/category> objects.

=head1 JSON

=begin javascript

	  {
		 "instance" : {
			"date_created" : "2018-02-27T09:39:28Z",
			"date_modified" : "2018-02-27T09:39:28Z",
			"name" : "Standaardteksten",
			"slug" : "standard_texts"
		 },
		 "reference" : "7ad36e82-e32d-4578-af74-2cf05696f644",
		 "type" : "config/category"
	  }

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 name E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

Localized name of the category for display.

=head2 slug E<raquo> L<C<text>|Zaaksysteem::Manual::API::V1::ValueTypes/text>

Internal slug of the category

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
