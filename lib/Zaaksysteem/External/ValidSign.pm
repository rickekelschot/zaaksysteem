package Zaaksysteem::External::ValidSign;
use Moose;
use namespace::autoclean;
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::External::ValidSign - A ValidSign integration module

=head1 DESCRIPTION

Integrate L<WebService::Validsign> into Zaaksysteem. This module is the glue
layer between any module that wants to use WebService::ValidSign into
Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::External::ValidSign;

    my $model = Zaaksysteem::External::ValidSign->new(

        secret                 => 'my-api-key'
        endpoint               => 'https://some.validsign.nl/api',
        subject                => Zaaksysteem::BR::Subject->new(),
        file_rs                => $schema->resultset('File'),
        object_subscription_rs => $schema->resultset('ObjectSubscription'),
        case_rs                => $schema->resultset('Zaak'),
        interface              => $interface,

        # optional
        default_sender         => 'user@validsign.nl',
    );


=cut

with 'MooseX::Log::Log4perl';

use BTTW::Tools;

use WebService::ValidSign;
use WebService::ValidSign::Types qw(WebServiceValidSignURI);
use WebService::ValidSign::Object::Document;
use WebService::ValidSign::Object::DocumentPackage;

use Zaaksysteem::Types qw(EmailAddress UUID);

=head1 ATTRIBUTES

=head2 validsign

A L<WebService::ValidSign> object, build for you by C<_build_validsign>.

=cut

has validsign => (
    is      => 'ro',
    isa     => 'WebService::ValidSign',
    builder => '_build_validsign',
    lazy    => 1,
);

=head2 secret

Your API key from ValidSign. Required.

=cut

has secret => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

=head2 default_sender

An optional default sender for ValidSign transactions, should be a valid
e-mailadress.  This sender will make sure that any user that isn't registered
at ValidSign can still ask others to sign documents.

=cut

has _default_sender => (
    is        => 'ro',
    isa       => EmailAddress,
    predicate => 'has_default_sender',
    init_arg  => 'default_sender',
);

=head2 endpoint

A ValidSign API endpoint. Required.

=cut

has endpoint => (
    is       => 'ro',
    isa      => WebServiceValidSignURI,
    required => 1,
    coerce   => 1,
);

=head2 subject

A L<Zaaksysteem::BR::Subject> object. Required.

=cut

has subject => (
    is       => 'ro',
    isa      => 'Zaaksysteem::BR::Subject',
    required => 1
);

=head2 object_subscription_rs

A L<Zaaksysteem::Backend::ObjectSubscription::ResultSet> object for creating
and search in Object Subscriptions. Required.

=cut

has object_subscription_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::ObjectSubscription::ResultSet',
    required => 1
);

=head2 file_rs

A L<Zaaksysteem::Backend::File::ResultSet> object for creating and searching
files in Zaaksysteem. Required.

=cut

has file_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::File::ResultSet',
    required => 1
);

=head2 case_rs

A L<Zaaksysteem::Zaken::ResultSetZaak> object for searching cases in
Zaaksysteem. Required.

=cut

has case_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Zaken::ResultSetZaak',
    required => 1
);

=head2 interface

An interface for the module so it can create/search for correct object
subscriptions.

=cut

has interface => (
    is       => 'ro',
    required => 1
);

has _callbacks => (
    is      => 'ro',
    isa     => 'HashRef',
    lazy    => 1,
    builder => '_build_callbacks',
);

=head2 get_sender

Get the sender based on the ID. If the sender cannot be found fall back on the
default sender. If that one cannot be found we die for real.

=cut

sig get_sender => 'Str'; # sig doesn't deal with UUID from ZS::Types

sub get_sender {
    my ($self, $id) = @_;

    if (!UUID->check($id)) {
        throw('validsign/sender/uuid', "Sender ID is not a UUID");
    }

    my $subject = $self->subject->find($id);
    my $email = $subject->subject->email_address;

    return try {
        $self->_get_sender(search => $email);
    }
    catch {
        my $err = $_;
        die $err unless $self->has_default_sender;
        return try { return $self->default_sender } catch { die $err };
    };
}

sub default_sender {
    my ($self) = @_;

    throw(
        "validsign/default_sender/missing",
        "You don't have a default sender defined!"
    ) unless $self->has_default_sender;

    return try {
        return $self->_get_sender(search => "" . $self->_default_sender);
    }
    catch {
        $self->log->error("Unable to get default sender!");
        die $_;
    };

}

=head2 get_file

Get a file from the filestore and make it a
L<WebService::ValidSign::Object::Document> for you to use.

=cut

sig get_file => 'Int';

sub get_file {
    my ($self, $id) = @_;

    my $file = $self->file_rs->find($id);

    throw("validsign/file/not_found", "Unable to find file with '$id'")
        unless $file;

    return WebService::ValidSign::Object::Document->new(
        id   => $file->filestore_id->uuid,
        name => $file->name,
        path => $file->filestore_id->get_path,
    );
}

=head2 get_case

Get a case based on the ID, if the case cannot be found or is closed we die.

=cut

sig get_case => 'Int';

sub get_case {
    my ($self, $id) = @_;

    my $case = $self->case_rs->find($id);

    if ($case && !$case->is_afgehandeld) {
        return $case;
    }
    elsif (!$case) {
        throw('validsign/case/not_found', "Unable to find case by ID $id");
    }
    else {
        throw('validsign/case/closed', "Unable to find open case by ID $id");
    }
}

=head2 create_signing_request

Implements all the logic to create a signing request. Returns a
L<WebService::ValidSign::Object::DocumentPackage> which has been sent to
ValidSign.

=cut

define_profile create_signing_request => (
    required => {
        case_id       => 'Int',
        file_id       => 'Int',
        subject_uuid  => UUID,
    },
);

sub create_signing_request {
    my $self    = shift;
    my $options = assert_profile({@_})->valid;

    my $sender   = $self->get_sender($options->{subject_uuid});
    my $case     = $self->get_case($options->{case_id});
    my $document = $self->get_file($options->{file_id});

    my $pkg = WebService::ValidSign::Object::DocumentPackage->new(
        name => sprintf(
            "Zaaksysteem zaak %d - file %s",
            $case->id, $document->name
        ),
        language => 'nl',
    );

    $pkg->sender($sender);
    $pkg->add_document($document);

    my $id = $self->validsign->package->create($pkg);

    $case->trigger_logging(
        'case/document/sign',
        {
            data => {
                pkg => {
                    name => $pkg->name,
                    id   => $pkg->id,
                },
                document_name  => $document->name,
                file_id        => $options->{file_id},
                interface_name => $self->interface->name,
            }
        }
    );

    my $os = $self->object_subscription_rs->create(
        {
            interface_id => $self->interface->id,
            external_id  => $pkg->id,
            local_table  => 'Zaak',
            local_id     => $case->id,
        }
    );

    return $pkg;
}

=head2 receive_event_cb

Execute a callback for the receive event.

=cut

sub receive_event_cb {
    my ($self, $sub) = @_;

    return try {
        return $sub->($self);
    }
    catch {
        $self->log->error("$_");
        die $_;
    }
}

=head2 get_object_subscription_by_external_id

Get an object subscription based based on the package ID from ValidSign. Dies
when nothing can be found.

=cut

sig get_object_subscription_by_external_id => 'Str';

sub get_object_subscription_by_external_id {
    my ($self, $id) = @_;

    my $os = $self->object_subscription_rs->search_rs(
        {
            local_table  => 'Zaak',
            interface_id => $self->interface->id,
            external_id  => $id,
        }
    )->first;
    return $os if $os;

    throw("validsign/receive_event/package_id/unknown",
        "Unabled to find package with ID '$id'"
    );
}


=head2 receive_event

Process a receive event.

=cut


sub receive_event {
    my ($self, $name) = @_;
    if (my $cb = $self->_callbacks->{$name}) {
        return $cb;
    }
    throw(
        "validsign/receive_event/type/unsupported",
        "Unable to process type '$name'"
    );
}

sub _build_validsign {
    my ($self) = @_;

    return WebService::ValidSign->new(
        secret   => $self->secret,
        endpoint => $self->endpoint,
    );
}

sub _get_sender {
    my $self    = shift;
    my $senders = $self->validsign->account->senders(@_);
    return $senders->[0] if @$senders == 1;

    if (@$senders > 1) {
        throw('validsign/senders/multiple',
            "Multiple senders found");
    }
    else {
        throw('validsign/senders/none',
            "No senders found");
    }
}

sub _build_callbacks {
    my $self = shift;
    return {
        PACKAGE_COMPLETE => sub {
            my ($self, %params) = @_;
            my $os = $self->get_object_subscription_by_external_id(
                $params{packageId}
            );

            my $case = $self->get_case($os->local_id);

            my $package = $self->validsign->package->find(
                $params{packageId}
            );

            my $fh = $self->validsign->package->download_document($package);
            my $file = $self->file_rs->file_create(
                {
                    name => sprintf("%s (ondertekend).pdf",
                        $package->documents->[0]->{name}),

                    file_path          => $fh->filename,
                    disable_logging    => 1,
                    disable_direct_vss => 1,
                    db_params          => { case_id => $case->id, },
                }
            );

            my $log_entry = $case->trigger_logging(
                'case/document/sign',
                {
                    data => {
                        action => 'package_complete',
                        pkg => {
                            name => $package->name,
                            id   => $package->id,
                        },
                        file_id        => $file->id,
                        document_name  => $file->name,
                        interface_name => $self->interface->name,
                    }
                }
            );

            $case->create_message_for_behandelaar(
                message    => "Er is een ondertekend document toegevoegd",
                event_type => $log_entry->event_type,
                log        => $log_entry,
            );

            $os->delete;
            return $case;
        },
        PACKAGE_DECLINE => sub {
            my ($self, %params) = @_;
            my $os = $self->get_object_subscription_by_external_id(
                $params{packageId}
            );
            my $case = $self->get_case($os->local_id);
            my $package = $self->validsign->package->find($params{packageId});
            $case->trigger_logging(
                'case/document/sign',
                {
                    data => {
                        action => 'package_decline',
                        pkg => {
                            name => $package->name,
                            id   => $package->id,
                        },
                        document_name  => $package->documents->[0]->name,
                        interface_name => $self->interface->name,
                    }
                }
            );
            $os->delete;
            return 1;
        },
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
