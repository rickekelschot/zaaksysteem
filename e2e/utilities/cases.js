export default [
	{
		caseNumber: 186,
		uuid: '607f5030-3369-4ff1-af81-fc6f01d23dc5'
	},
	{
		caseNumber: 187,
		uuid: '765b182d-4f26-4ac9-94de-0e7bb5d3ccd4'
	},
	{
		caseNumber: 194,
		uuid: '83dd2d95-063b-4aad-8de4-b6a4f1ebf4b8'
	},
	{
		caseNumber: 196,
		uuid: '4b7822c4-4966-41b8-b3dd-d71cee953265'
	},
	{
		caseNumber: 197,
		uuid: '990e9a8c-4d2a-443e-a987-6a1c6e56c25a'
	}
];

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
