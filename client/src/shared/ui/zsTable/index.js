import angular from 'angular';
import template from './template.html';
import zsTableBodyModule from './zsTableBody';
import zsTableHeaderModule from './zsTableHeader';
import './tables.scss';

export default
	angular.module('zsTable', [
		zsTableBodyModule,
		zsTableHeaderModule
	])
		.directive('zsTable', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					columns: '&',
					rows: '&',
					onColumnClick: '&',
					onRowClick: '&',
					useHref: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

					let ctrl = this;

					ctrl.handleColumnClick = ( columnId ) => {
						ctrl.onColumnClick({ $columnId: columnId });
					};
					

				}],
				controllerAs: 'zsTable'
			};


		}])
		.name;
