import angular from 'angular';
import zsRolePickerModule from './../../ui/zsRolePicker';
import assign from 'lodash/fp/assign';
import template from './template.html';
import defaultCapabilities from './defaultCapabilities';

export default
	angular.module('rightPicker', [
		zsRolePickerModule
	])
		.component('rightPicker', {
			template,
			bindings: {
				right: '<',
				onRightChange: '&'
			},
			controller: [ function ( ) {

				let ctrl = this;
				
				ctrl.defaultCapabilities = defaultCapabilities();

				ctrl.handleRoleChange = ( unit, role ) => {

					ctrl.onRightChange({
						$right: assign(ctrl.right, {
							position: {
								unit,
								role
							}
						})
					});

				};

				ctrl.handleCapabilityClick = ( name ) => {

					ctrl.onRightChange({
						$right: assign(ctrl.right, {
							capabilities: ctrl.getCapabilities().map(
								capability => {
									return capability.name === name ?
										assign(capability, { selected: !capability.selected })
										: capability;
								}
							)
						})
					});

				};

				ctrl.getCapabilities = ( ) => (ctrl.right && ctrl.right.capabilities) || ctrl.defaultCapabilities;

			}]
		})
		.name;
