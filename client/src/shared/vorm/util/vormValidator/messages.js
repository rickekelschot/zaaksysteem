import dateformat from 'dateformat';

export default
	{
		email: 'Er is geen geldig e-mailadres ingevuld (Voorbeeld: naam@example.com)',
		number: 'Er is geen valide getal ingevuld. Alleen cijfers zijn toegestaan. (Voorbeeld: 12345678)',
		valuta: 'Er is geen bedrag ingevuld. (Voorbeeld: 123,45)',
		url: 'Er is geen valide web adres ingevuld. (Voorbeeld: http://www.voorbeeld.nl of www.voorbeeld.nl)',
		bankaccount: 'Er is geen geldig IBAN-rekeningnummer ingevuld (Voorbeeld: NL91ABNA0417164300)',
		minDate: [ '$minDate', ( $minDate ) => {
			return `De eerst mogelijke datum is ${dateformat($minDate, 'dd-mm-yyyy')}.`;
		}],
		maxDate: [ '$maxDate', ( $maxDate ) => {
			return `De laatst mogelijke datum is ${dateformat($maxDate, 'dd-mm-yyyy')}.`;
		}],
		required: 'Dit veld is verplicht.',
		requiredOrInvalid: 'Dit veld is verplicht of onjuist ingevuld.'
	};
