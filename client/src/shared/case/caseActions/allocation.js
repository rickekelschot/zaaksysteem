import get from 'lodash/get';
import keyBy from 'lodash/keyBy';
import merge from 'lodash/merge';
import pick from 'lodash/pick';

import ZaaksysteemException from '../../exceptionHandling/ZaaksysteemException';

import propCheck from '../../util/propCheck';

import {
  translateErrorToUI,
} from '../../exceptionHandling/error';

export {
  assertCaseAllocationData,
  transformAllocationParams,
  getComment,
  getCaseDistributors,
  setCaseAllocation,
  rejectCaseAllocation,
  confirmCaseRejectAllocation,
};

/* Assert the data that goes into the case allocation call
 *
 * @data {Hash} - The allocation data
 */
function assertCaseAllocationData(data) {
  propCheck.throw(
    propCheck.shape({
      caseId: propCheck.number,
      allocationType: propCheck.oneOf([ 'assignee', 'assign_to_self', 'org-unit' ]),
      assigneeId: data.allocationType !== 'org-unit' ? propCheck.any : propCheck.any.optional,
      changeDepartment: data.allocationType !== 'org-unit' ? propCheck.bool : propCheck.bool.optional,
      orgUnitId: data.allocationType === 'org-unit' ? propCheck.any : propCheck.any.optional,
      roleId: data.allocationType === 'org-unit' ? propCheck.any : propCheck.any.optional,
      notify: data.allocationType === 'assignee' ? propCheck.bool : propCheck.any.optional,
      comment: propCheck.any.optional,
    }),
    data
  );
}

/* Transform the allocation parameters to something
 *
 * @values {Hash} allocation values from the UI
 * @user   {UserObject} The loggined in user
 * @return {Hash} transformed allocation values for the internal API
 */
function transformAllocationParams(values, user) {
  const alloc = values.allocation_type;
  if (alloc === 'org-unit') {
    return {
      'orgUnitId': values['org-unit'].unit,
      'roleId': values['org-unit'].role,
      'notify' : false,
    };
  }
  else {
    return {
      'assigneeId': alloc === 'assignee' ? values.assignee.id : user.id,
      'notify': alloc === 'assignee' ? Boolean(values.send_email) : false,
      'changeDepartment': Boolean(values.change_department),
    };
  }
}

/* Get the comment data from the UI
 */
function getComment(values) {
  let value = get(values, 'comment');
  if (value !== undefined) {
    value = value.trim();
    if (value.length) {
      return { comment: value };
    }
  }
  return {};
}

/*  setCaseAllocation
 *  Set the case allocation data for the POST
 *  to /zaak/:caseID/update/allocation
 *
 *  TODO: Look into client/src/shared/case/caseActions/index.js
 *  and check if the mutation of the data needs to be done in one
 *  place and not multiple like it is now.
 *
 * @mutationData - Case allocation data
 */
function setCaseAllocation(mutationData) {

  const caseId = mutationData.caseId;

  /* Be aware the calls are posted and not changes to JSON therefor
   * the backend doesn't parse Booleans as booleans
   */
  let params = {
    selected_case_ids: [ caseId ],
    selection: 'one_case',
    context: 'case',
    commit: 1,
  };

  params = merge(params, getComment(mutationData));

  if (mutationData.allocationType === 'org-unit') {
    params.change_allocation = 'group';
    params.ou_id = mutationData.orgUnitId;
    params.role_id = mutationData.roleId;
  }
  else {
    params.betrokkene_id     = `betrokkene-medewerker-${mutationData.assigneeId}`;
    params.change_allocation = 'behandelaar';
    params.change_department =  mutationData.changeDepartment ? 1 : 0;
    params.notify            =  mutationData.notify ? 1 : 0;
  }

  return {
    url: `/zaak/${caseId}/update/allocation`,
    params,
    method: 'POST'
  };
}

/* Reject case allocation call
 *
 */

function rejectCaseAllocation(object) {
  object.snackbar.wait('Toewijzing wordt geweigerd', {
    promise: object.http({
      url: `/api/v1/case/${object.caseUUID}/reject`,
      method: 'POST',
      data: object.postData,
    }),
    then: ( ) => 'Toewijzing is geweigerd',
    catch: (response) => {
      return translateErrorToUI(response);
    }
  })
    .then(( ) => {
      object.resultResource();
    });
}

/* Get the case distributors from the ZS backend
 *
 * returns an object that has two keys, group and role. Both correspond
 * to their respective objects.
 */

function getCaseDistributors(settings) {
  const prefix = 'case_distributor_';
  let defaults = pick(
    keyBy(settings, setting => setting.reference.replace(prefix, '')),
    'group', 'role');

  if (defaults.group === undefined || defaults.role === undefined) {
    throw new ZaaksysteemException(
      'case/reject/incomplete_distributors',
      'Case distributors are not set in the configuration'
    );
  }
  return {
    group : defaults.group.instance.value.instance,
    role : defaults.role.instance.value.instance,
  };
}

/* Display the confirm case rejection UI to the user and
 * process their response
 *
 * Their response is handled in part by zsConfirm which is injected to
 * this method. There is no return value, only the snackbar gets
 * updated.
 */

function confirmCaseRejectAllocation(object) {
  try {
    const distributors = getCaseDistributors(object.settings);

    const text = `De geweigerde zaak wordt toegewezen aan ${distributors.group.name}/${distributors.role.name}.`;

    const inputObjects = [
      {
        label    : 'Plaats opmerking',
        input    : 'text',
        required : false,
        name     : 'comment',
      },
    ];

    object.confirm(text, 'Zaak weigeren', inputObjects).then(( response ) => {
        rejectCaseAllocation(merge(object, { postData : response }));
    });

  }
  catch (err) {
    object.snackbar.error(translateErrorToUI(err));
  }
}
