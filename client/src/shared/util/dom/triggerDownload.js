import createElement from './createElement';
import mimeTypes from '../../mimeTypes';

export default function triggerDownload(response, filename) {
  const ext = filename.split('.').pop();
  const file = new Blob([response.data], { type: mimeTypes[ext] });

  if (window.navigator.msSaveOrOpenBlob === undefined) {
    const URL = ('URL' in window) ?
      window.URL
      : window.webkitURL;
    const blobUrl = URL.createObjectURL(file);
    const body = document.body;
    const anchor = createElement('a', {
      href: blobUrl,
      download: filename
    });
    
    body.appendChild(anchor);
    anchor.click();
    body.removeChild(anchor);
    URL.revokeObjectURL(blobUrl);
  } else {
    window.navigator.msSaveOrOpenBlob(file, filename);
  }
}
