import angular from 'angular';
import zsCaseRegistrationModule from './../../../shared/case/zsCaseRegistration';

export default angular.module('Zaaksysteem.intern.register', [
		zsCaseRegistrationModule
	])
	.name;
