import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import values from 'lodash/values';
import find from 'lodash/find';
import includes from 'lodash/includes';
import each from 'lodash/each';
import merge from 'lodash/merge';
import pickBy from 'lodash/pickBy';

const TIMEOUT = 5000;

export default ( options ) => {

	let { $http, $q, $timeout, opts, ids } = options,
		deferred = { },
		pending = [];

	opts = merge(
		{},
		opts,
		{
			params: {
				rows_per_page: 100
			}
		}
	);

	let retry = ( ) => {

		return $http(opts)
			.success(( data ) => {

				each(deferred, ( d, reference ) => {

					let job = find(data.result.instance.rows, { reference });

					if (
						includes(pending, d)
						&& (!job || job.instance.status === 'failed' || job.instance.status === 'finished')
					) {

						if (job && job.instance.status === 'failed') {
							d.reject(job);
						} else {
							d.resolve(job);
						}
						pending = pending.filter(pendingDeferred => pendingDeferred !== d);
					}

				});

				if (pending.length) {
					$timeout(retry, TIMEOUT);
				}

			});

	};

	if (ids) {
		deferred = mapValues(keyBy(ids), ( ) => $q.defer());
		$timeout(retry, TIMEOUT);

		pending = values(deferred);

		return $q.when(pending.map(def => def.promise));
	}

	return $http(opts)
		.then( ( response ) => {

			deferred =
				mapValues(
					pickBy(
						keyBy(response.data.result.instance.rows, 'reference'),
						( job ) => job.instance.status !== 'failed' && job.instance.status !== 'finished'
					),
					( ) => $q.defer()
				);

			$timeout(retry, TIMEOUT);

			pending = values(deferred);

			return pending.map(d => d.promise);

		});

};
