import angular from 'angular';
import template from './template.html';
import zsTimelineModule from '../../../../../shared/ui/zsTimeline';
import './styles.scss';

export default
	angular.module('zsContactTimelineView', [
        zsTimelineModule
	])
		.component('zsContactTimelineView', {
			bindings: {
				subject: '&'
			},
			controller: [ function ( ) {

				let ctrl = this;

				ctrl.getTimelineReference = ( ) => ctrl.subject().data().reference;

			}],
			template
		})
		.name;
