import angular from 'angular';
import template from './template.html';
import resourceModule from '../../../../../shared/api/resource';
import composedReducerModule from '../../../../../shared/api/resource/composedReducer';
import seamlessImmutable from 'seamless-immutable';
import zsDropdownMenuModule from '../../../../../shared/ui/zsDropdownMenu';
import zsCaseWidgetModule from '../../../../../shared/ui/zsCaseWidget';
import map from 'lodash/map';
import get from 'lodash/get';
import mapValues from 'lodash/mapValues';
import find from 'lodash/find';
import identity from 'lodash/identity';
import first from 'lodash/first';
import some from 'lodash/some';
import getFullName from '../../../../../shared/util/subject/getFullName';

import './styles.scss';

export default
	angular.module('zsContactCasesView', [
		composedReducerModule,
		resourceModule,
		zsDropdownMenuModule,
		zsCaseWidgetModule
	])
		.component('zsContactCasesView', {
			bindings: {
				subject: '&'
			},
			controller: [ '$scope', '$element', '$timeout', 'resource', 'composedReducer', 'rwdService', function ( scope, $element, $timeout, resource, composedReducer, rwdService ) {

				let ctrl = this,
					caseWidgetOptionsReducer,
					currentlySelectedOptionReducer,
					caseDataResource,
					caseDataReducer,
					compactReducer,
					rowsPerPage = 10,
					searchOpen = false,
					selectedWidgetOption = {
							owned: true,
							related: false,
							authorized: false,
							shared_address: false
						};

				caseWidgetOptionsReducer = composedReducer( { scope }, ( ) => selectedWidgetOption, ctrl.subject() )
					.reduce( ( options, subject ) => {

						let addressObj = subject.instance.subject.instance.address_residence,
							address =
								get(addressObj, 'instance.street')
								&& get(addressObj, 'instance.street_number') ? // ZS-TODO: Change this to instance.reference when backend makes sure the reference is filled when address information is known.
									`${addressObj.instance.street || ''} ${addressObj.instance.street_number || ''} ${addressObj.instance.street_number_letter || ''} ${addressObj.instance.street_number_suffix || ''}`
									: null;

						const name = getFullName(subject);

						return map(options, ( value, key ) => {

							let label;

							switch (key) {
								case 'owned':
								label = `Zaken van ${name}`;
								break;
							
								case 'related':
								label = 'Betrokken bij zaken';
								break;

								case 'authorized':
								label = 'Gemachtigd voor zaken';
								break;

								case 'shared_address':
								label = `Zaken op ${address}`;
								break;
							}

							return !address && key === 'shared_address' ?
								null
								: {
									name: key,
									label,
									click: ( ) => {
										ctrl.selectOption(key);
									},
									selected: value
								};

						}).filter(identity);

					});

				ctrl.getCaseWidgetOptions = caseWidgetOptionsReducer.data;

				currentlySelectedOptionReducer = composedReducer({ scope }, caseWidgetOptionsReducer)
					.reduce( options => {
						return find(options, ( option ) => option.selected).label;
					});

				ctrl.getCurrentlySelectedCaseWidgetOption = currentlySelectedOptionReducer.data;

				ctrl.selectOption = ( filter ) => {
					selectedWidgetOption = mapValues(selectedWidgetOption, ( ) => false);
					selectedWidgetOption[filter] = true;
					caseDataResource.reload();
				};

				caseDataResource = resource(
					( ) => {

						let keys = Object.keys(selectedWidgetOption),
							activeOption = first(keys.filter( key => selectedWidgetOption[key])),
							searchPartial = ctrl.searchQuery ? `MATCHING "${ctrl.searchQuery}"` : '',
							url = `/api/v1/subject/${ctrl.subject().data().reference}/cases/${activeOption}`,
							zql = `SELECT {} FROM case ${searchPartial} NUMERIC ORDER BY case.number DESC`;

						return ctrl.subject().data().reference && activeOption ? {
							url,
							params: {
								rows_per_page: rowsPerPage,
								zql
							}
						}
						: null;

					},
					{ scope })
						.reduce( ( requestOptions, data ) => {
							return data ? data : seamlessImmutable([]);
						});

				caseDataReducer = composedReducer({ scope }, caseDataResource )
					.reduce( casesFromResource => casesFromResource );

				ctrl.getCaseWidgetData = caseDataReducer.data;

				compactReducer = composedReducer({ scope }, rwdService.getActiveViews)
					.reduce( activeViews => {
						return !!(some(activeViews, ( v ) => v.indexOf('medium-and-down') !== -1));
					});

				ctrl.isCompact = compactReducer.data;

				ctrl.isLoading = ( ) => caseDataReducer.state() === 'pending';

				ctrl.isFetching = ( ) => caseDataReducer.state() === 'pending' || caseDataResource.fetching();

				ctrl.isEmpty = ( ) => get(caseDataResource.data(), 'length') === 0;

				ctrl.getCurrentPage = ( ) => caseDataResource.cursor();
				
				ctrl.hasNextPage = ( ) => !!caseDataResource.next();
				
				ctrl.hasPrevPage = ( ) => !!caseDataResource.prev();

				ctrl.getLimit = ( ) => caseDataResource.limit();

				ctrl.handleLimitChange = ( limit ) => {
					rowsPerPage = limit;
					caseDataResource.request(caseDataResource.cursor(1));
					caseDataResource.reload();
				};

				ctrl.handlePageChange = ( page ) => {
					caseDataResource.request(caseDataResource.cursor(page));
				};

				ctrl.toggleSearch = ( ) => {
					searchOpen = !searchOpen;

					if (searchOpen) {
						$timeout(( ) => {
							$element.find('input')[0].focus();
						}, 0, false);
					} else {
						ctrl.searchQuery = '';
					}
				};

				ctrl.handleSearchKeyUp = ( event ) => {

					switch (event.keyCode) {
						case 27:
						searchOpen = false;
						ctrl.searchQuery = '';
						break;
					}

				};

				ctrl.isSearchOpen = ( ) => searchOpen;

				ctrl.handleSearchQuery = ( ) => caseDataResource.reload();

				ctrl.reloadData = ( ) => caseDataResource.reload();

				scope.$on('$destroy', ( ) => {

					[ caseDataResource ].forEach(res => {
						res.destroy();
					});

				});

			}],
			template
		})
		.name;
