import {
	getValue
} from './format';

const summary = true;

export default [
	{
		summary,
		key: 'first_names',
		label: 'Voornamen',
		getValue
	},
	{
		summary,
		key: 'surname',
		label: 'Achternaam',
		getValue
	}
];
