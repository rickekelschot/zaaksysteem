package TestFor::General::Backend::Message;

use TestSetup;

use Zaaksysteem::Types qw/Betrokkene/;

use base qw(ZSTest Zaaksysteem::Backend::Message::Roles::Test);

=head1 NAME

TestFor::General::Backend::Message::Component - Zaaksysteem Message Component

=head1 SYNOPSIS

    ./zs_prove -v t/lib/TestFor/General/Backend/Message.pm

=head1 LAYOUT

This file implements all USAGE tests, in other words: if you would like
to use the engine, here is your inspiration.

=head1 USAGE (or end-to-end)

Usage tests: use the below tests when you would like to give this module
a spin.

=head2 message_usage_create_message

Creates a message and checks the load on the database

=cut

=head1 IMPLEMENTATION

=head2 message_to_json

Verify TO_JSON

=cut

sub message_to_json : Tests {
    my $self        = shift;

    my $jsonconfig  = {
        required        => {
            aanvrager       => 'Str',
            case_type       => 'Str',
            id              => 'Int',
            logging_id      => 'HashRef',
            message         => 'Str',
            subject_id      => Betrokkene,
        },
        optional        => {
            created_by      => 'Str',
            is_read         => 'Int',
        }
    };

    $zs->zs_transaction_ok(
        sub {
            my $message = $self->create_message();
            my $json    = $message->TO_JSON;

            ok(length $json->{ $_ }, "Found JSON attr: $_") for keys %{ $jsonconfig->{required} };
            ok(exists $json->{ $_ }, "Found JSON attr: $_") for keys %{ $jsonconfig->{optional} };
        },
        'Creation of message tested'
    );

    $zs->zs_transaction_ok(
        sub {
            $self->create_message() for (1..2);

            my @messages = $schema->resultset('Message')->search->with_related_json();

            ### Check first message, check query count
            my $json;
            $zs->num_queries_ok(
                sub {
                    $json = $messages[0]->TO_JSON;
                },
                0
            );

            ok(length $json->{ $_ }, "Found JSON attr: $_") for keys %{ $jsonconfig->{required} };
            ok(exists $json->{ $_ }, "Found JSON attr: $_") for keys %{ $jsonconfig->{optional} };
        },
        'Viewing of multiple messages valid'
    );

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
