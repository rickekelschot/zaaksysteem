package TestFor::Catalyst::Controller::API::V1::Controlpanel::Host;
use base qw(ZSTest::Catalyst);

use Moose;
use TestSetup;

=head1 NAME

TestFor::Catalyst::Controller::API::V1::Controlpanel::Host - Proves the boundaries of our API: Controlpanel Host

=head1 SYNOPSIS

    See USAGE tests

    Quick test of this single file from within vagrant
    ZS_DISABLE_STUF_PRELOAD=1 ./zs_prove -v t/lib/TestFor/Catalyst/Controller/API/V1/Controlpanel/Host.pm

=head1 DESCRIPTION

These tests prove the interactions between the outside servicebus and our zaaksysteem. This test
uses the version 1 API of zaaksysteem, and proves the C<api/v1/controlpanel/UUID/Host> namespace.

=head1 USAGE

Usage tests, use these if you would like to know how to interact with the API, it's also useful
for extended documentation of the StUF API.

=head2 Create

=head3 create controlpanel host

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/create/8989asdf-asdf-asdf89a89sa-asdf89/host/create

B<Request>

=begin javascript

{
   "fqdn" : "mijn.example.com",
   "ip" : "127.0.0.44",
   "label" : "productie host"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-c7cf9e-f4676b",
   "result" : {
      "instance" : {
         "fqdn" : "mijn.example.com",
         "id" : "221a5015-d427-4853-b045-98a8d4311e9f",
         "ip" : "127.0.0.44",
         "label" : "productie host",
         "owner" : "betrokkene-bedrijf-194"
      },
      "reference" : "221a5015-d427-4853-b045-98a8d4311e9f",
      "type" : "host"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_host_create : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'mijn.example.com',
                label            => 'productie host',
                ip               => '127.0.0.44',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $instance = $self->valid_host_object($cp_data);

    }, 'api/v1/controlpanel/host/create: create a simple controlpanel host');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn            => 'test.zaaksysteem.nl',
                label           => 'Testomgeving',
                otap            => 'production',
                protected       => 1,
            }
        );

        my $json          = $mech->content;
        my $instance_data = $self->_get_json_as_perl($json);

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'mIJn.example.com',
                label            => 'productie host',
                ip               => '127.0.0.44',
                instance         => $instance_data->{result}->{instance}->{id},
            }
        );

        $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $host = $self->valid_host_object($cp_data);
        like($host->{fqdn}, qr/^[a-z\.]+$/, 'Got a lowercase hostname');

        ok(keys %{ $host->{instance} }, 'Got a filled instance object');

    }, 'api/v1/controlpanel/host/create: create with an instance');
}

=head3 update controlpanel host

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/create/8989asdf-asdf-asdf89a89sa-asdf89/host/221a5015-d427-4853-b045-98a8d4311e9f/update

B<Request>

=begin javascript

{
   "fqdn" : "moir.example.com",
   "ip" : "127.0.0.55",
   "label" : "productie host 2"
}

=end javascript

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-b91bee-4ed05b",
   "result" : {
      "instance" : {
         "fqdn" : "moir.example.com",
         "id" : "cc7f627f-d3d3-4648-83de-594faeb65901",
         "ip" : "127.0.0.55",
         "label" : "productie host 2",
         "owner" : "betrokkene-bedrijf-265",
         "ssl_cert" : null,
         "ssl_key" : null
      },
      "reference" : "cc7f627f-d3d3-4648-83de-594faeb65901",
      "type" : "host"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_host_update : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'mijn.example.com',
                label            => 'productie host',
                ip               => '127.0.0.44',
            }
        );

        my $json     = $mech->content;
        my $cp_data  = $self->_get_json_as_perl($json);

        my $host     = $self->valid_host_object($cp_data);

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/' . $host->{id} . '/update',
            {
                fqdn             => 'moir.example.com',
                label            => 'productie host 2',
                ip               => '127.0.0.55',
            }
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);

        $host     = $self->valid_host_object($cp_data);

        is($host->{fqdn}, 'moir.example.com', 'Got changed host: fqdn');
        is($host->{label}, 'productie host 2', 'Got changed host: label');
        is($host->{ip}, '127.0.0.55', 'Got changed host: ip');

    }, 'api/v1/controlpanel/host/update: update a simple controlpanel host');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/instance/create',
            {
                fqdn            => 'test.zaaksysteem.nl',
                label           => 'Testomgeving',
                otap            => 'production',
                protected       => 1,
            }
        );

        my $json          = $mech->content;
        my $instance_data = $self->_get_json_as_perl($json);

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'mijn.example.com',
                label            => 'productie host',
                ip               => '127.0.0.44',
            }
        );

        $json         = $mech->content;
        my $cp_data   = $self->_get_json_as_perl($json);

        my $host      = $self->valid_host_object($cp_data);

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/' . $host->{id} . '/update',
            {
                fqdn             => 'moir.example.com',
                label            => 'productie host 2',
                ip               => '127.0.0.55',
                instance         => $instance_data->{result}->{instance}->{id},
            }
        );

        $json     = $mech->content;
        $cp_data  = $self->_get_json_as_perl($json);

        $host     = $self->valid_host_object($cp_data);

        is($host->{fqdn}, 'moir.example.com', 'Got changed host: fqdn');
        is($host->{label}, 'productie host 2', 'Got changed host: label');
        is($host->{ip}, '127.0.0.55', 'Got changed host: ip');
        ok(keys %{ $host->{instance} }, 'Got a filled instance object');

        my $r             = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/instance/' . $instance_data->{result}->{instance}->{id});
        $instance_data    = $self->_get_json_as_perl($mech->content);

        is(@{ $instance_data->{result}->{instance}->{hosts}->{rows} }, 1, 'Got one host instance');
    }, 'api/v1/controlpanel/host/update: add a contropanel host to an instance');
}

=head2 Get

=head3 get controlpanel host

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel/8989asdf-asdf-asdf89a89sa-asdf89/host/9205e6b9-bfed-4f4f-aaa8-aef39bf2e790

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-c7cf9e-f4676b",
   "result" : {
      "instance" : {
         "fqdn" : "mijn.example.com",
         "id" : "221a5015-d427-4853-b045-98a8d4311e9f",
         "ip" : "127.0.0.44",
         "label" : "productie host",
         "owner" : "betrokkene-bedrijf-194"
      },
      "reference" : "221a5015-d427-4853-b045-98a8d4311e9f",
      "type" : "host"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_instance_get : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'mijn.example.com',
                label            => 'productie host',
                ip               => '127.0.0.44',
            }
        );

         my $json     = $mech->content;
         my $cp_data  = $self->_get_json_as_perl($json);

         my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/host/' . $cp_data->{result}->{reference});
         $json       = $mech->content;

         $self->valid_host_object($self->_get_json_as_perl($json));

         # print STDERR $json;

    }, 'api/v1/controlpanel/get: get a simple controlpanel');
}

=head2 List

=head3 get controlpanel host listing

    # curl --anyauth -k -H "Content-Type: application/json" --digest -u "zaaksysteem:abcdefghijklmnop123qrs" https://localhost/api/v1/controlpanel

B<Response>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-b819d1-ad30f0",
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "fqdn" : "mijn.example.com",
                  "id" : "53e764fd-813b-4227-8596-3fa0540b4334",
                  "ip" : "127.0.0.44",
                  "label" : "productie host",
                  "owner" : "betrokkene-bedrijf-198"
               },
               "reference" : "53e764fd-813b-4227-8596-3fa0540b4334",
               "type" : "host"
            },
            {
               "instance" : {
                  "fqdn" : "test.example.com",
                  "id" : "69a9e0eb-f67d-43d7-9ef6-4756133e3400",
                  "ip" : "127.0.0.45",
                  "label" : "productie host",
                  "owner" : "betrokkene-bedrijf-198"
               },
               "reference" : "69a9e0eb-f67d-43d7-9ef6-4756133e3400",
               "type" : "host"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=cut

sub cat_api_v1_controlpanel_host_list : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
         my $mech     = $zs->mech;
         my $controlp = $self->_create_controlpanel;

         $mech->post_json(
             $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'test.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
         );

         $mech->post_json(
             $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'mijn.example.com',
                label            => 'productie host',
                ip               => '127.0.0.44',
            }
         );

         my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/host');
         my $json    = $mech->content;

         my $cp_data = $self->_get_json_as_perl($json);

         is($cp_data->{result}->{type}, 'set', 'Got a set of answers');

         $self->valid_host_object($cp_data);

         print STDERR $json;

    }, 'api/v1/controlpanel: list all of controlpanel objects of zaaksysteem');
}

=head1 IMPLEMENTATION TESTS

=head2 cat_api_v1_controlpanel_host_exceptions

=cut

sub cat_api_v1_controlpanel_host_exceptions : Tests {
    my $self    = shift;

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'test.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'test.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );
        $self->validate_json_exception($mech, qr/Object unique constraint violation: fqdn, ip/, 'Exception: controlpanel host/ip exists');
    }, 'api/v1/controlpanel/create: exceptions');

    $zs->zs_transaction_ok(sub {
        my $mech      = $zs->mech;
        my $controlp  = $self->_create_controlpanel;
        my $controlp2 = $self->_create_controlpanel;

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'test.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp2->{id} . '/host/create',
            {
                fqdn             => 'test2.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );

        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp2->{id} . '/host/' . $cp_data->{result}->{instance}->{id} . '/update',
            {
                fqdn             => 'test.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );

        $self->validate_json_exception($mech, qr/Object unique constraint violation: fqdn, ip/, 'Exception: controlpanel host fqdn exists');
    }, 'api/v1/controlpanel/update: exceptions');

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        ### No host
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'test2.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );

        my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/host/123456');
        # my $r       = $mech->get('/api/v1/controlpanel/' . $cp_data->{result}->{reference});
        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        is($mech->status, '404', 'Got valid exception error');
        is($cp_data->{status_code}, '404', 'Got valid status_code');
        is($cp_data->{result}->{type}, 'exception', 'Got exception result');
        like($cp_data->{result}->{instance}->{message}, qr/The controlpanel host object with UUID.*not be found/, 'Exception: controlpanel instance could not be found');
    });

    $zs->zs_transaction_ok(sub {
        my $mech     = $zs->mech;
        my $controlp = $self->_create_controlpanel;

        ### No instance
        $mech->post_json(
            $mech->zs_url_base . '/api/v1/controlpanel/' . $controlp->{id} . '/host/create',
            {
                fqdn             => 'test2.example.com',
                label            => 'productie host',
                ip               => '127.0.0.45',
            }
        );

        my $r       = $mech->get('/api/v1/controlpanel/' . $controlp->{id} . '/host/' . $controlp->{id});
        # my $r       = $mech->get('/api/v1/controlpanel/' . $cp_data->{result}->{reference});
        my $json    = $mech->content;
        my $cp_data = $self->_get_json_as_perl($json);

        is($mech->status, '404', 'Got valid exception error');
        is($cp_data->{status_code}, '404', 'Got valid status_code');
        is($cp_data->{result}->{type}, 'exception', 'Got exception result');
        like($cp_data->{result}->{instance}->{message}, qr/The controlpanel host object with UUID.*not be found/, 'Exception: controlpanel instance could not be found');
    });
}

sub validate_json_exception {
    my $self    = shift;
    my $mech    = shift;
    my $regex   = shift;
    my $message = shift;

    my $json     = $mech->content;
    my $cp_data  = $self->_get_json_as_perl($json);

    is($mech->status, '500', 'Got valid exception error');
    is($cp_data->{status_code}, '500', 'Got valid status_code');
    is($cp_data->{result}->{type}, 'exception', 'Got exception result');
    like($cp_data->{result}->{instance}->{message}, $regex, $message);
}

sub valid_host_object {
    my $self      = shift;
    my $perl      = shift;

    my $instance = $perl->{result}->{instance};
    if ($perl->{result}->{type} eq 'set') {
        $instance = $perl->{result}->{instance}->{rows}->[0]->{instance};
    } else {
       is($perl->{result}->{type}, 'host', 'Found controlpanel instance');
       like($perl->{result}->{reference}, qr/^\w+\-\w+\-/, 'Got a "reference", like "f87e9f29-5720-4743-9cc5-47887b342709"');
       isa_ok($perl->{result}->{instance}, 'HASH', 'Got an host of controlpanel');
    }

    like($instance->{fqdn}, qr/^\w+\.[\w+\.]+$/, 'Got a valid value for fqdn');
    like($instance->{label}, qr/^[\w\s]+$/, 'Got a valid value for label');
    like($instance->{ip}, qr/^\d+\.\d+\.\d+\.\d+$/, 'Got a valid IP address');
    like($instance->{owner}, qr/^betrokkene-bedrijf-\d+$/, 'Got a valid value for owner');

    return $instance;
}

sub _get_json_as_perl {
    my $self    = shift;
    my $json    = shift;

    my $jo      = JSON->new->utf8->pretty->canonical;

    return $jo->decode($json);
}

sub _create_controlpanel {
    my $self     = shift;

    $zs->create_interface_ok(
        module => 'controlpanel'
    );

    my $mech     = $zs->mech;

    my $company  = $zs->create_bedrijf_ok;

    $mech->zs_login;
    $mech->post_json(
        $mech->zs_url_base . '/api/v1/controlpanel/create',
        {
            owner            => 'betrokkene-bedrijf-' . $company->id,
            customer_type    => 'government',
        }
    );

    my $json     = $mech->content;
    my $cp_data  = $self->_get_json_as_perl($json);

    return $cp_data->{result}->{instance};
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
