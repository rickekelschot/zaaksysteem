BEGIN;
    -- For graphs
    CREATE INDEX ON object_data(hstore_to_timestamp(index_hstore->'case.date_of_completion'))
        WHERE defined(index_hstore, 'case.date_of_completion');

    CREATE INDEX ON object_data(CAST(hstore_to_timestamp(index_hstore->'case.date_of_registration') AS DATE));
    CREATE INDEX ON object_data(CAST(hstore_to_timestamp(index_hstore->'case.date_of_completion') AS DATE))
        WHERE defined(index_hstore, 'case.date_of_completion');

    CREATE INDEX ON object_data( (index_hstore->'case.route_ou') )
        WHERE defined(index_hstore, 'case.route_ou');

    CREATE INDEX ON object_data(
        date_part('year', hstore_to_timestamp(index_hstore->'case.date_of_registration')),
        date_part('month', hstore_to_timestamp(index_hstore->'case.date_of_registration'))
    );
COMMIT;
