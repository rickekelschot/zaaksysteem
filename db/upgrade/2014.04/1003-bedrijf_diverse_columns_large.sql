BEGIN;

ALTER TABLE bedrijf ALTER COLUMN vestiging_adres TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN vestiging_straatnaam TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN vestiging_postcodewoonplaats TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN vestiging_woonplaats TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN correspondentie_adres TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN correspondentie_straatnaam TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN correspondentie_postcodewoonplaats TYPE TEXT;
ALTER TABLE bedrijf ALTER COLUMN correspondentie_woonplaats TYPE TEXT;


ALTER TABLE gm_bedrijf ALTER COLUMN vestiging_adres TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN vestiging_straatnaam TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN vestiging_postcodewoonplaats TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN vestiging_woonplaats TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN correspondentie_adres TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN correspondentie_straatnaam TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN correspondentie_postcodewoonplaats TYPE TEXT;
ALTER TABLE gm_bedrijf ALTER COLUMN correspondentie_woonplaats TYPE TEXT;

COMMIT;