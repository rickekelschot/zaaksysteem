BEGIN;
    CREATE INDEX ON zaak_betrokkenen(zaak_id);
    CREATE INDEX ON zaak_betrokkenen(betrokkene_type, betrokkene_id);
    CREATE INDEX ON zaak(aanvrager);
    CREATE INDEX ON zaak(behandelaar);
    CREATE INDEX ON zaak(coordinator);
    CREATE INDEX ON zaak(pid);
COMMIT;
