BEGIN;

DROP TABLE IF EXISTS seen;
DROP TABLE IF EXISTS kennisbank_relaties;
DROP TABLE IF EXISTS kennisbank_producten;
DROP TABLE IF EXISTS kennisbank_vragen;

COMMIT;
