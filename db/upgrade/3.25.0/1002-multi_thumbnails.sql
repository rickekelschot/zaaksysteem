BEGIN;

CREATE TABLE file_thumbnail (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id INTEGER NOT NULL REFERENCES file(id),
    filestore_id INTEGER NOT NULL REFERENCES filestore(id),
    max_width INTEGER NOT NULL,
    max_height INTEGER NOT NULL,
    date_generated TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);

COMMIT;
