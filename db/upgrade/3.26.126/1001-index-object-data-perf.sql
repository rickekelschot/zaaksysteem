

DROP INDEX IF EXISTS object_data_object_uuid_idx;
CREATE INDEX CONCURRENTLY object_data_object_uuid_idx ON object_data( (index_hstore->'object.uuid') );

DROP INDEX IF EXISTS object_data_case_channel_of_contact_idx;
CREATE INDEX CONCURRENTLY object_data_case_channel_of_contact_idx ON object_data( (index_hstore->'case.channel_of_contact') );
