BEGIN;

UPDATE zaak_bag
    SET
          bag_type = 'nummeraanduiding'
        , bag_id = bag_nummeraanduiding_id
    WHERE
        bag_type = 'verblijfsobject'
        AND bag_nummeraanduiding_id IS NOT NULL;

COMMIT;