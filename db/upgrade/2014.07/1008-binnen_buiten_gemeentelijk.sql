BEGIN;

ALTER TABLE natuurlijk_persoon ADD COLUMN in_gemeente BOOLEAN;

UPDATE natuurlijk_persoon SET in_gemeente = true WHERE authenticated = true;

COMMIT;