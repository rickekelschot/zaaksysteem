#!/usr/bin/perl -w

use Moose;
use Data::Dumper;

use Cwd 'realpath';

use FindBin;
use lib "$FindBin::Bin/../../lib";

use Catalyst qw[ConfigLoader];

use Catalyst::Log;
use Catalyst::Model::DBIC::Schema;

my $log = Catalyst::Log->new;

error("USAGE: $0 [dsn] [user] [password] [commit]") unless @ARGV && scalar(@ARGV) >= 1;
# info("\n\nLET OP!!! LET OP!!! LET OP!!! LET OP!!!");
# info("Script enkel draaien wanneer koppelingen PRS/NNP/ADR/etc gereed zijn\n\n");

my ($dsn, $user, $password, $commit)    = @ARGV;
my $dbic                                = database($dsn, $user, $password);

$dbic->txn_do(sub {
    migrate_sleutels($dbic);

    unless ($commit) {
        die("ROLLBACK, COMMIT BIT not given\n");
    }
});

$log->info('All done.');
$log->_flush;

sub database {
    my ($dsn, $user, $password) = @_;

    my %connect_info = (
        dsn             => $dsn,
        pg_enable_utf8  => 1,
    );

    $connect_info{password}     = $password if $password;
    $connect_info{user}         = $user if $user;


    Catalyst::Model::DBIC::Schema->config(
        schema_class => 'Zaaksysteem::Schema',
        connect_info => \%connect_info
    );

    Catalyst::Model::DBIC::Schema->new->schema;
}

sub error {
    $log->error(shift);
    $log->_flush;

    exit;
}

sub info {
    $log->info(sprintf shift, @_);
    $log->_flush;
}

sub migrate_sleutels {
    my $dbic    = shift;

    ### Check dat koppelingen zijn gebouwed
    if (my $interface = $dbic->resultset('Interface')->find_by_module_name('stufprs')) {
        migrate_natuurlijk_persoon($dbic, $interface);
    }

    if (my $interface = $dbic->resultset('Interface')->find_by_module_name('stufnnp')) {
        migrate_company($dbic, $interface);
    }
}

sub migrate_natuurlijk_persoon {
    my $dbic                        = shift;
    my $interface                   = shift;

    my $entries                     = $dbic->resultset('NatuurlijkPersoon')->search(
        {
            authenticated   => 1,
            authenticatedby => 'gba',
            system_of_record_id => { '!=' => undef },
        }
    );

    my $counter = 0;
    while (my $entry = $entries->next) {
        info(
            sprintf("%-7d", ++$counter) . ': Migrating entry "'
            . $entry->geslachtsnaam
            . '" ['
            . $entry->burgerservicenummer . ']: '
            . $entry->system_of_record_id
        );

        $interface->object_subscriptions->create(
            {
                local_table     => 'NatuurlijkPersoon',
                local_id        => $entry->id,
                date_created    => DateTime->now,
                object_preview  => $entry->voornamen . ' ' . (
                        $entry->voorvoegsel
                            ? $entry->voorvoegsel . ' '
                            : ''
                    ) . $entry->geslachtsnaam,
                external_id     => $entry->system_of_record_id,
            }
        );
    }
}

sub migrate_company {
    my $dbic                        = shift;
    my $interface                   = shift;

    my $entries                     = $dbic->resultset('Bedrijf')->search(
        {
            authenticated   => 1,
            authenticatedby => 'kvk',
            system_of_record_id => { '!=' => undef },
        }
    );

    my $counter = 0;
    while (my $entry = $entries->next) {
        info(
            sprintf("%-7d", ++$counter) . ': Migrating entry "'
            . $entry->handelsnaam
            . '" ['
            . $entry->fulldossiernummer . ']: '
            . $entry->system_of_record_id
        );

        $interface->object_subscriptions->create(
            {
                local_table     => 'Bedrijf',
                local_id        => $entry->id,
                date_created    => DateTime->now,
                object_preview  => $entry->handelsnaam,
                external_id     => $entry->system_of_record_id,
            }
        );
    }
}

# sub delete_unknown_gegevensbeheer_entries {
#     my $dbic    = shift;

#     ### Check dat koppelingen zijn gebouwed
#     if ($dbic->resultset('Interface')->find_by_module_name('stufprs')) {
#         delete_unknown_natuurlijk_persoon($dbic);
#     }

#     if ($dbic->resultset('Interface')->find_by_module_name('stufnnp')) {
#         delete_unknown_company($dbic);
#     }
# }

# sub delete_unknown_natuurlijk_persoon {
#     my $dbic                = shift;

#     my $subscriptions       = $dbic->resultset('ObjectSubscription')->search_active(
#         {
#             'local_table'       => 'NatuurlijkPersoon'
#         }
#     );

#     my $entries             = $dbic->resultset('NatuurlijkPersoon')->search(
#         {
#             deleted_on      => undef,
#             authenticated   => 1,
#             authenticatedby => 'gba',
#             id              => { 'not in' => $subscriptions->get_column('id')->as_query }
#         }
#     );

#     info('Marking number of NatuurlijkPersoon as deleted: ' . $entries->count);

#     my $now     = DateTime->now();

#     while (my $entry = $entries->next) {
#         if ($entry->adres_id) {
#             my $adres = $entry->adres_id->deleted_on($now);
#             $adres->update;
#         }

#         $entry->deleted_on($now);
#         $entry->update;
#         info('Deleted: ' . $entry->geslachtsnaam . ' / ' . $entry->burgerservicenummer);
#     }
# }

# sub delete_unknown_company {
#     my $dbic                = shift;

#     my $subscriptions       = $dbic->resultset('ObjectSubscription')->search_active(
#         {
#             'local_table'       => 'Bedrijf'
#         }
#     );

#     my $entries             = $dbic->resultset('Bedrijf')->search(
#         {
#             deleted_on      => undef,
#             authenticated   => 1,
#             authenticatedby => 'kvk',
#             id              => { 'not in' => $subscriptions->get_column('id')->as_query }
#         }
#     );

#     info('Marking number of Bedrijf as deleted: ' . $entries->count);

#     my $now     = DateTime->now();

#     while (my $entry = $entries->next) {
#         info('Deleted: ' . $entry->handelsnaam . ' / ' . $entry->dossiernummer);
#         $entry->deleted_on($now);
#         $entry->update;
#     }
# }

1;
