#!/usr/bin/env perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use File::Basename;
use File::Spec::Functions;
use Time::HiRes qw(gettimeofday tv_interval);
use Try::Tiny;
use Zaaksysteem::CLI;

my $cli = try {
    Zaaksysteem::CLI->init;
}
catch {
    die $_;
};

my %opt = %{ $cli->options };

if ($opt{casetype} && $opt{offset}) {
    warn "Conflicting options: casetype and offset";
    pod2usage(1) ;
}

my $dbic = $cli->schema;;

my $args = {};
if (exists $opt{casetype}) {
    $args = { 'me.id' => $opt{casetype} };
}

my $rs = $dbic->resultset('Zaaktype')->search(
    $args,
    {
        order_by => { -desc => 'me.id' },
        $opt{offset} ? (offset => $opt{offset}) : (),
    }
);


my $count = $rs->count();
if (!$count) {
    print "No case types found\n";
    return;
}

my $starttime = [gettimeofday];
my $casetypes_done = 0;
while(my $zaaktype = $rs->next) {
    $casetypes_done++;
    $cli->do_transaction(
        sub {
            my $action = $zaaktype->deleted ? "Deleting" : "Updating";
            printf(
                "%s hstore for zaaktype %d (%d of %d, %.3f/second)\n",
                $action,
                $zaaktype->id,
                $casetypes_done, $count,
                $casetypes_done / tv_interval($starttime, [gettimeofday]),
            );
            $zaaktype->_sync_object(Zaaksysteem::Object::Model->new(
                table => 'ObjectData',
                schema => $dbic
            ));
        }
    );
}

1;

__END__

=head1 NAME

touch_casetype.pl - A casetype hstore fixer

=head1 SYNOPSIS

touch_case.pl OPTIONS [ [ --casetypes 1234 ] [--casetypes 12345 ] ]

=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * customer_d

The customer_d dir, defaults to the relative directory where zaaksysteem.conf is found.

=item * hostname

The hostname you want to touch cases for

=item * casetype

You can use this to update a specific case type, not supplying this will touch all case types.

=item * offset

In case you want to touch all casetypes but want to start with a certain offset. This is handy in case the process get's killed and you want to continue the run.
This option conflicts with the casetype option.

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
