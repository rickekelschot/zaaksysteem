FROM jeantil/openstack-swift-keystone-docker:pike

RUN export OS_USERNAME=admin && \
    export OS_PASSWORD=7a04a385b907caca141f && \
    export OS_PROJECT_NAME=admin && \
    export OS_USER_DOMAIN_NAME=Default && \
    export OS_PROJECT_DOMAIN_NAME=Default && \
    export OS_AUTH_URL=http://127.0.0.1:35357/v3 && \
    export OS_IDENTITY_API_VERSION=3 && \
    apachectl start && \
    openstack endpoint create --region RegionOne object-store internal http://127.0.0.1:8080/v1/KEY_%\(tenant_id\)s && \
    openstack endpoint create --region RegionOne object-store admin http://127.0.0.1:8080/v1 && \
    openstack endpoint create --region RegionOne object-store public http://swift:8080/v1/KEY_%\(tenant_id\)s

COPY docker/inc/swift/entrypoint.sh /swift/bin/entrypoint.sh
