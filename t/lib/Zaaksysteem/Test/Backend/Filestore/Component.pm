package Zaaksysteem::Test::Backend::Filestore::Component;
use Zaaksysteem::Test;

use feature 'state';
use Zaaksysteem::DocumentConverter;
use Zaaksysteem::Schema::Filestore;
use Zaaksysteem::Backend::Filestore::Component;

=head1 NAME

Zaaksysteem::Test::Backend::Filestore::Component - Test the Filestore DB component

=head1 DESCRIPTION

Tests for the Filestore database component.

=head1 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Filestore::Component

=cut

sub test_generate_thumbnail_single_conversion {
    note "Making a thumbnail of a file directly to PNG";

    my $fs = Zaaksysteem::Schema::Filestore->new({
        mimetype => 'text/plain',
    });

    my $override = override(
        'Zaaksysteem::Backend::Filestore::Component::get_path',
        sub {
            return "get_path";
        },
    );

    my $converter_control = override( 'Zaaksysteem::DocumentConverter::can_convert'=> sub {
            my ($self, %args) = @_;

            is($args{source_type}, 'text/plain', 'Source type is passed correctly to can_convert');
            is($args{destination_type}, 'image/png', 'Destination type is passed correctly to can_convert');

            # For simple conversion, return true:
            return 1;
        })->override('Zaaksysteem::DocumentConverter::convert_file' => sub {
            my ($self, %args) = @_;

            is($args{source_filename},  'get_path',  'Source path is passed to convert_file correctly');
            is($args{destination_type}, 'image/png', 'Destination type for "simple" conversion is PNG immediately');
            is($args{filter_options}{width}, 42, "Width passed correctly to converter");
            is($args{filter_options}{height}, 31337, "Height passed correctly to converter");

            open my $h, ">", $args{destination_filename};
            print $h "Thumbnail goes here";
            close $h;
        },
    );
    my $converter = Zaaksysteem::DocumentConverter->new();
    $fs->_converter($converter);

    my $thumb_temp = $fs->generate_thumbnail(
        width  => 42,
        height => 31337,
    );

    my $thumb_data = <$thumb_temp>;
    is($thumb_data, "Thumbnail goes here", "Thumbnail was written to file correctly");
}

sub test_generate_thumbnail_double_conversion {
    note "Making a thumbnail of a file by turning it into a PDF file first";

    my $fs = Zaaksysteem::Schema::Filestore->new({
        mimetype => 'text/plain',
    });

    my $override = override(
        'Zaaksysteem::Backend::Filestore::Component::get_path',
        sub {
            return "get_path";
        },
    );

    my $converter_control = override('Zaaksysteem::DocumentConverter::can_convert' => 
        => sub {
            my ($self, %args) = @_;
            state $pass = 1;

            is($args{source_type}, 'text/plain', 'Source type is passed correctly to can_convert');
            if ($pass == 1) {
                is($args{destination_type}, 'image/png', 'Destination type PDF is passed correctly to can_convert');
                $pass += 1;
                return 0;
            }
            elsif ($pass == 2) {
                is($args{destination_type}, 'application/pdf', 'Destination type PDF is passed correctly to can_convert');
                $pass += 1;
                return 1;
            }

            fail("Unknown call to can_convert - expected 2 calls, got at least '$pass'");
        })->override(
        'Zaaksysteem::DocumentConverter::convert_file' => sub {
            my ($self, %args) = @_;

            state $pass = 1;
            state $pdf_filename;

            if ($pass == 1) {
                is($args{source_filename},  'get_path',        'Source path is passed to convert_file correctly');
                is($args{destination_type}, 'application/pdf', 'Destination type for "double" conversion is PDF first');

                $pdf_filename = $args{destination_filename};
                $pass++;
            }
            elsif ($pass == 2) {
                is($args{source_filename},  $pdf_filename,  'Temporary PDF path is passed to convert_file correctly');
                is($args{destination_type}, 'image/png', 'Destination type for "double" conversion is PNG second');
                is($args{filter_options}{width}, 42, "Width passed correctly to converter");
                is($args{filter_options}{height}, 31337, "Height passed correctly to converter");
            }

            open my $h, ">", $args{destination_filename};
            print $h "Thumbnail goes here";
            close $h;
        },
    );
    my $converter = Zaaksysteem::DocumentConverter->new();
    $fs->_converter($converter);

    my $thumb_temp = $fs->generate_thumbnail(
        width  => 42,
        height => 31337,
    );

    my $thumb_data = <$thumb_temp>;
    is($thumb_data, "Thumbnail goes here", "Thumbnail was written to file correctly");
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
